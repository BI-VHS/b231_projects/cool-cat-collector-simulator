# Dynamický Svět
## Animace

Klíčovým prvkem jsou kočičky. Jistě není překvapivé, že námi vytvořené animace jsou zaměřené přímo na ně.

![silly__1_](/uploads/178f4e14a93af8f6efaa0934fa50c357/silly__1_.gif)

![image](/uploads/a0be000307c9cdb42cb82df28334c5da/image.png)

![silly__3_](/uploads/00be9a953ce9939f923466e192833b9a/silly__3_.gif)

Pří vývoji se ovšem vyskytlo několik problému exportování animací a modelů z Blenderu. V repozitáři lze tedy najít i (nakonec bohužel) nevyužité animace.

![image](/uploads/0a562b8d616aaa620754687caabcd113/image.png)

Pro pestrost animací jsme si poradili i "kódovou" animací pohybu s hraním si s vektory v prostoru. Příkladem mohou být poletující motýlci:

![silly__2_](/uploads/e7e3b994d87d9892199e7f951ac2d736/silly__2_.gif)

A nebo svalnatá dřevorubecká lesní kočička, která seká dříví pouze silou vytvořenou vlastním tělem:

![image](/uploads/2af8b8ae966d325e870b818e253c3533/image.png)


## Cyklické činnosti, Chování, Unikáty, Produkce

### Malá koťátka na louce

Koťátka si ráda hrají, v noci spí a přes den různě pobíhají. Jsou ale velice hyperaktivní a při zaběhnutí do sebemenší travičky nadskočí metr vysoko.

Spánek -> ... -> Stát -> Rotace -> Chůze -> (Možný skok) -> ... -> Spánek

![image](/uploads/f1e86869a15390fb3877a493e51e9f19/image.png)


### Kočka sběratelka

Tato kočka se ráda prochází. V cestě ji ovšem překáží velký plot.

![image](/uploads/364f0547019984b9f987ac45767173ce/image.png)

Naneštěstí pro ní, plot stráží kamenná kočka, která za cenu bobule ráda kohokoli pustí na druhou stranu.

![image](/uploads/8e7a30bb72f5cda37ffa6a86f326f64a/image.png)

Chůze -> Sběr bobulí -> Výměna bobulí kamenné kočce za vstup na druhou stranu -> Chůze

Pozn.: Pro náznak sběru bobulí je použita proměna barvy kočičky. Stejně tak při jejich odevzdání kamenné kočičce (zde dochází ke změně na původní barvu).

![image](/complexBehaviour.gif)

### Lesní kočičky

Vzhledem k jejich skryté lokaci se nemusí obávat žádných predátorů. Svůj den rády tráví pozorováním okolí a v noci zase tepla příbytků.

Ohlížení se -> Spánek v příbytku -> Ohlížení se

![image](/uploads/ac0e85115c3d085e35c654e6a3c9791c/image.png)


### Kočka Světla

Jistě nebude překvapením, že kočka světla není leda jak obyčejná. Vyjma její procházkou myšlení osvítí přeci každý nápad. Pokud jí pohladíte, nápady se rozzáříte. Aby toho nebylo málo, během pohlazení se ráda protáčí a pokud se tak stane dokonce přímo v blízkosti sněhuláka, celý se z toho ozlatí.

Chůze -> Rotace -> (Ozlacení sněhuláka) -> Chůze

![image](/uploads/da72594de540f11e492385e96d32cf02/image.png)


### Kočka lásky

Dalším unikátní kočičkou, dokonce přímo v zóně louky, je kočička lásky. Sama jí naprosto přečnívá, ale nedává to příliš najevo. Pokud dojde k intenzivnímu hlazení, prozradí se ve všech směrech

Chůze -> <3 -> Chůze

![image](/uploads/1b9658efd44f09c323a590397e95d79a/image.png)

![image](/uploads/0d1019cca7501dc8d897847a9493a31c/image.png)


### Další lesní kočky

I v populaci lesních kočiček se najdou výjimky, ve smyslu každodenního chování. Svalnaté duo, dřevorubecká a truhlářská kočička, spolupracují v explicitním produkčním řetězci s jediným cílem a to obohatit již tak krásný les ještě více lavičkami na sezení.

Jelikož se jedná o poctivou spolupráci, bez ohledu na to, která z kočiček dokončí své poslání první (sekání dříví, postavení lavičky), počkají na sebe vždy u ohniště. A jelikož se nejedná o roboty, doby jednotlivých činností jsou proměnlivé (a pro nás, lidi, na oko náhodné)

![image](/uploads/15776dfee5d20b1cf18e69322683f26a/image.png)

Čekání -> Kácení dřeva + Postavení lavičky -> Čekání


### Další produkce

Důležitou produkcí je hlavní mechanika hry. Pro pokračování v dobrodružství je nutné získat více Meows a to lze jediným způsobem, hlazením kočiček.

Za Meows lze jednak odemykat portály do dalších zón, pořizovat si kočičky jako mazlíčky, ale také provádět "rebirths".

Rebirth, neboli znovuzrození, je pokročilou mechanikou, která umožňuje znovu projít celým dobrodružstvím od začátku. Nicméně aby se dobrodružství od předešlého odlišilo, leč se jedná z principu o totéž, jsou za to uděleny bonusy jako je větší množství Meows, prémiová měna rubíny a dokonce možné, zatím neobjevené, zóny, do kterých se v původním dobrodružství nebylo možné dostat. 


### Denní cyklus

Vyjma hor, kde nelze nikdy přesně určit čas, se ve všech zónách mění denní cyklus. Na ten zpravidla kočičky reagují, pokud neuznají, že jejich činnost má například před spánkem přednost.

