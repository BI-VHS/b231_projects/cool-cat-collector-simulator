# Komplexní Svět
## Dialogy

Téměř každá kočička má nějaká slova na srdci... kromě lesních, ty si příliš nepovídají.

![image](/uploads/7ef0c7575ccb2bb6ef062db760076bcd/image.png)

![image](/uploads/760a4386effe8276454aed0b5cdaddb1/image.png)

Někdy se jedná o jednoduché vyjádření myšlenek, jindy zase o nápovědu k hlubšímu porozumění:

![image](/uploads/a0700885caa51abe43807eb04173dde2/image.png)


Klíčovou osobou v příběhu, respektive klíčovou kočičkou, je zlatá kočička hned z kraje louky. Ta vede nejen ráda delší dialogy, ale dokonce vyhodnocuje rozhodnutí dobrodruhů.

![image](/uploads/2089c680add426c5731ad24b9d8bfbf0/image.png)

Pokud by dobrodruh odmítnul podstoupit pouť plnou kočiček a osvícení, bude jeho život velice rychle ukončen (exploduje). Na druhou stranu kočička dává ráda druhé, třetí, čtvrté, páté... jednoduše řečeno hodně dalších šancí a tak se lze v příštím životě dobrodruha rozhodnout jinak.

Pozn.: Roblox v defaultním dialogovém systému nedovoluje rozsáhlejší funkcionalitu a tak bylo třeba použít vlastních scriptů. Času bohužel na plnohodnotný dialog systém nezbyl a tak vzhledem k nepřekonaným limitacím existujícího Roblox dialog systému nejsou všechny zprávy celé vidět.

<details><summary>Zde je celý úryvek zamýšleného dialogu se zlatou kočičkou</summary>

One evening, after a long day at work, you found yourself mysteriously transported to a world like no other. You were surrounded by vibrant landscapes filled with giant yarn balls, catnip trees, and endless stretches of cozy sunbeams.

"Welcome to Catworld. Oh, long-awaited hero, we are sorry for bringing you here so suddenly, but we purr-ed for someone to help us," said a mysterious cat near you.

"Eh? Bruh, there is a talking cat. Am I still sleeping?"

"It is but a dream. But if you are not sure if it is reality, please, at least fulfill the wish of a talking cat. The feline inhabitants of Catworld, each possessing unique abilities and personalities, were divided into different factions, squabbling over territory and resources. The once-harmonious land was now in chaos, and the only hope for peace rested on your shoulders."

1. "Wow, I dreamed of being a hero. What is this if not a chance presented by fate? But… how can I help?"

"You are unbiased. All the cats can possibly accept you because you only act by yourself and for the sake of everyone. And I am willing to help you on your journey."

"I will try my best!"

2. "Nah, I don’t feel like this. I'd better collect all the cats and conquer this entire world mwahahaha, or as you would say meow-hahaha?"

"Forget all I said. From now on, for you, I am just a cat. Meow meow meow." Refuses to elaborate further. Chadpick related.  

Guided by a wise and regal Cat Elder named Whiskerpaw, you learned that you are the "Chosen One" destined to unite the cats of Catworld. Your mission is to bring together the feline factions, forging alliances and restoring balance to the purr-fect realm.

You journeyed through the vast landscapes of Catworld, meeting quirky cat characters along the way. There was Sir Fluffington, a chivalrous knight with a heart of gold; Luna, a mysterious and elegant black cat skilled in the art of shadow-play; and Whiskers the Inventor, a clever tabby cat with a knack for crafting ingenious contraptions.

Each faction presented its own set of challenges, requiring you to use wit, charm, and, most importantly, love for cats to unite them. Along the way, you discovered that the key to unity was celebrating the unique qualities of each cat, fostering understanding, and finding common ground.

As you progressed, Catworld transformed before your eyes. The once-divided land began to flourish with cooperation and camaraderie. The yarn balls and catnip trees thrived, and the sunbeams shone even brighter.

Eventually, you faced the ultimate challenge – a dark force that sought to sow discord among the cats. With the power of unity and the bonds you had forged, you confronted the malevolent presence and restored harmony to Catworld.

As a token of gratitude, the Cat Elder bestowed upon you a magical trinket that would allow you to return to the human world. However, the love you had gained from your feline friends and the lessons of unity you had learned stayed with you forever.

And so, with a heart full of fond memories and a newfound appreciation for the bonds that can be forged between species, you bid farewell to Catworld, leaving behind a land of purr-fect harmony.

Back in the human world, you couldn't help but smile whenever you saw a cat, knowing that in another realm, your feline friends were living in peace, side by side, united by the Paws of Unity.

</details>

## Příběh

Po dlouhém dni v práci jste se najednou ocitli v záhadném Kočičím světě. Jste "Vyvolený" k obnově míru v tomto kouzelném světě. S pomocí různých kočiček se vydáváte na cestu, s cílem poražení temné síly, která se snaží narušit rovnováhu tohoto roztomilého kočičího světa. Na konci vašeho velkého huňatého dobrodružství vás čeká osvícení, nekončící láska ke kočkám, nezapomenutelné zážitky a taky cesta zpět do lidského světa.

Podaří se vám zachránit mír v kočičím světě?


## Úkoly a předměty

Hlavním úkolem je rozšiřovat množství Meows, postupovat do dalších zón a následně opakovat své dobrodružství za pomocí mechaniky Rebirths.

Kalkulace probíhá následovně:

```lua
local RebirthsConfig = {}

RebirthsConfig.BasePrice = 800
RebirthsConfig.IncreasePerRebirth = 175

function RebirthsConfig.CalculatePrice(currentRebirth: number, amountoFrebirths: number?)
	amountoFrebirths = if amountoFrebirths then amountoFrebirths else 1
	
	local price = 0
	local rebirths = 0
	while (rebirths < amountoFrebirths) do
		price += RebirthsConfig.BasePrice + ((rebirths+currentRebirth) * RebirthsConfig.IncreasePerRebirth)
		rebirths += 1
	end
	
	return price
end

return RebirthsConfig
```

Jako předměty by se daly počítat mazlíčci, tedy kočičky které lze získat výměnou za Meows nebo Rubíny. Jejich myšlenka je taková, že vás následují na každém vašem kroku a pozitivně přispívají k získanému množství Meows.