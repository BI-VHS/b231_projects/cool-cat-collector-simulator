# Cool Cat Collector Simulator

@rohankry 
@akimovl1 

## Dokumentace

- [Statický Svět](https://gitlab.fit.cvut.cz/BI-VHS/b231_projects/cool-cat-collector-simulator/-/blob/master/StatickySvet.md)
- [Dynamický Svět](https://gitlab.fit.cvut.cz/BI-VHS/b231_projects/cool-cat-collector-simulator/-/blob/master/DynamickySvet.md)
- [Komplexní Svět](https://gitlab.fit.cvut.cz/BI-VHS/b231_projects/cool-cat-collector-simulator/-/blob/master/KomplexniSvet.md)

## Trailer

![video](/cat.mp4)

*Doplňen později

## Game

[Test Here!](https://www.roblox.com/games/14903517072/WIP-Cat-Land-Simulator)

[Watch here! (gameplay footage, HUD disabled)](https://drive.google.com/file/d/1nxYneT0hF3797eR4L7yPNBH4G0y-4IGZ/view?usp=sharing)
