# Statický Svět


## Scény
Svět je jako takový rozdělený do několika separátních zón. Jednotlivé zóny představují prostor pro jiné druhy kočiček, které jej oplývají svojí existencí. Pro vstup do následující zóny je však nutné splnit minimální množství Meows, tedy klíčové měny, bez které nelze dokázat svojí hodnost osvícení o existenci vzácnějších druhů koček. Pro přechody jsou použity magické portály. V následujícím diagramu je vyobrazeno konceptuální schéma:

![image](/uploads/ffd9a4b9761e601b089a6546fa163a8d/image.png)

- Louka
- Les
- Hory
- Ostrov


### Louka

Louka představuje počáteční a klíčovou scénu pro každého dobrodruha. Oplývá pozitivní energií na každém rohu a ve svém středu, srdci, zahrnuje jezero mládí. 

![image](/uploads/6f0749873360164c23cea337c19518c6/image.png)

Díky jednoduchosti umožňuje snadný a hlavně přímočarý způsob pochopení základních mechanik.


### Les

Les oproti louce již každý dobrodruh nespatří. Jedná se o skrytější zónu, ve které však bydlí neobvyklé lesní kočičky.

![image](/uploads/783f5972303b6cccc6d32cf18aed2235/image.png)


### Hory

Hory činí velice neobvyklé místo pro kočičky. Nenechte se ovšem zmýlit, výšiny vědomostí si pro svou existenci bedlivě uvážila vzácná kočička světla, která svou nekonečnou procházkou myšlení osvítí každý nápad... a pak také jeden milý sněhulák.

![image](/uploads/8d2c2a38ea973f9b35e4fc3d5562a1c6/image.png)


### Ostrov

Tato scéna zatím nebyla objevena. Ovšem z pověstí plyne, že si ostrov pro své pohodlí a relaxující atmosféru zvolily mystické legendární kočičky pocházející z říše Atlantidy.


## Assety

Velké množství použitých modelů bylo tvořeno Roblox komunitou a jsou veřejně dostupné v developer katalogu (jejich použití je z právního hlediska TOS katalogu možné za předpokladu nevydávání za své):

![image](/uploads/073c4881da52075bec14282cfa59a597/image.png)

Použity byly napříč všemi scénami, občas s úpravami pro zachování zamýšlené atmosféry a celkové experience z dané scény.